﻿using Common;
using DTO;
using ElasticSearch.Core;
using IAccess;
using RobotMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using Nest;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAccess
{
    public class PlatformAccess : AccessCore<Entity.Platform, PlatformQuery>, IPlatformAccess
    {
        protected override string OrderByName => "creatTime";

        public void AddPlatform(DTO.Platform platform)
        {
            if (platform == null) return;
            var count = Client.Search<Entity.Platform>(s =>
                 s.Query(q =>
                     q.Term(t =>
                         t.Name, platform.Name))).Total;
            if (count >= 1)
                throw new BaseCustomException($"名称为{platform.Name}的平台已存在!");

            count = Client.Search<Entity.Platform>(s =>
                 s.Query(q =>
                     q.Term(t =>
                         t.Config.AppId, platform.AppId))).Total;
            if (count >= 1)
                throw new BaseCustomException($"AppId为{platform.AppId}的平台已存在!");

            var entity = platform.RobotMap<Platform, Entity.Platform>();
            entity.Id = Guid.NewGuid().ToString();
            var res = base.Add(entity);
            if (!res.IsValid)
                throw new BaseCustomException("新增失败！");
        }

        public void DeletePlatform(List<string> ids)
        {
            SearchProvider.DeleteByIds<Entity.Platform>(ids);
        }

        public Platform GetPlatformByAppId(string appId)
        {
            if (string.IsNullOrEmpty(appId)) return null;
            var entity = Client.Search<Entity.Platform>(s =>
                 s.Query(q =>
                     q.Term(t =>
                         t.Config.AppId, appId))).Documents.FirstOrDefault();
            if (entity == null)
                return null;
            return entity.RobotMap<Entity.Platform, Platform>();
        }

        public Platform GetPlatformByAppSecrect(string appId, string secrect)
        {
            var entity = Client.Search<Entity.Platform>(s =>
                s.Query(q =>
                    q.Term(t =>
                        t.Config.AppId, appId) &&
                    q.Term(t =>
                        t.Config.AppSecrect, secrect)))
                .Documents.FirstOrDefault();

            if (entity == null)
                return null;
            return entity.RobotMap<Entity.Platform, Platform>();
        }

        public List<Platform> QueryPlatform(PlatformQuery query)
        {
            var entities = PageQuery(query);
            return entities.RobotMap<Entity.Platform, Platform>();
        }

        protected override List<Func<QueryContainerDescriptor<Entity.Platform>, QueryContainer>> CreatQueryContainer(PlatformQuery query)
        {
            var querys = new List<Func<QueryContainerDescriptor<Entity.Platform>, QueryContainer>>();

            if (!string.IsNullOrEmpty(query.Name))
                querys.Add(t => t.Wildcard("name.keyword", $"*{query.Name}*"));
            if (!string.IsNullOrEmpty(query.AppId))
                querys.Add(s => s.Term("appId.keyword", query.AppId));
            return querys;
        }
    }
}
