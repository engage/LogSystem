﻿using Common;
using Configuration;
using ElasticSearch.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElasticSearchAccess
{
    public class BusinessAccess
    {
        private PlatformAccess _PlatformAccess = new PlatformAccess();

        public string IfNotInAddReturnId(string appId, string businessPosition)
        {
            if (string.IsNullOrEmpty(appId))
                throw new ArgumentException(nameof(appId), $"{nameof(appId)}不允许为空！");

            string businessLink = Constants.DefaultBusiness;
            if (!string.IsNullOrEmpty(businessPosition))
                businessLink = businessPosition;

            //先根据AppId获取业务平台Id
            var platform = _PlatformAccess.GetPlatformByAppId(appId);
            if (platform == null)
                throw new AppIdInvalidException(appId);

            var entity = SearchProvider.ElasticClient<Entity.Business>().Search<Entity.Business>(s =>
                 s.Query(q =>
                     q.Term(t =>
                         t.BusinessLink, businessLink) &&
                     q.Term(t =>
                         t.PlatformId, platform.Id)))
                .Documents.FirstOrDefault();
            if (entity != null)
                return entity.Id.ToString();

            //不存在则插入
            var newEntity = new Entity.Business()
            {
                PlatformId = platform.Id,
                PlatformName = platform.Name,
                BusinessLink = businessLink,
                Id = Guid.NewGuid().ToString()
            };
            SearchProvider.Insert(newEntity);
            return newEntity.Id.ToString();
        }

        public Entity.Business IfNotInAddReturnEntity(string appId, string businessPosition)
        {
            if (string.IsNullOrEmpty(appId))
                throw new ArgumentException(nameof(appId), $"{nameof(appId)}不允许为空！");

            string businessLink = Constants.DefaultBusiness;
            if (!string.IsNullOrEmpty(businessPosition))
                businessLink = businessPosition;

            //先根据AppId获取业务平台Id
            var platform = _PlatformAccess.GetPlatformByAppId(appId);
            if (platform == null)
                throw new AppIdInvalidException(appId);

            var entity = SearchProvider.ElasticClient<Entity.Business>().Search<Entity.Business>(s =>
                 s.Query(q =>
                     q.Term(t =>
                         t.BusinessLink, businessLink) &&
                     q.Term(t =>
                         t.PlatformId, platform.Id)))
                .Documents.FirstOrDefault();
            if (entity != null)
                return entity;

            //不存在则插入
            var newEntity = new Entity.Business()
            {
                PlatformId = platform.Id,
                BusinessLink = businessLink,
                PlatformName = platform.Name,
                Id = Guid.NewGuid().ToString(),
            };
            SearchProvider.Insert(newEntity);
            return newEntity;
        }
    }
}
